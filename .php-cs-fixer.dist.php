<?php

$finder = PhpCsFixer\Finder::create()
    ->in('api-client-php_sources')
    ->exclude('var/')
;

$config = new PhpCsFixer\Config();
return $config->setFinder($finder);
