<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ServerDialog
{
    private HttpClientInterface $client;
    private ContainerBagInterface $params;

    /**
     * Class constructor for: ServerDialog
     *
     * @param HttpClientInterface $client
     * @param ContainerBagInterface $params
     */
    public function __construct(
        HttpClientInterface   $client,
        ContainerBagInterface $params
    ) {
        $this->client = $client;
        $this->params = $params;
    }

    /**
     * Make an API call to Asqatasun Server
     *
     * @param string $requestType Type of request: GET, POST, DELETE
     * @param string $requestPath Path to the API. Example: /api/v0/audit
     * @param array $requestOptions All options needed for the request.
     * @return JsonResponse
     */
    public function apiCall(
        string $requestType,
        string $requestPath,
        array  $requestOptions
    ): JsonResponse {
        $asqa_url = $this->params->get('app.asqatasunServer.url');
        $asqa_username = $this->params->get('app.asqatasunServer.username');
        $asqa_password = $this->params->get('app.asqatasunServer.password');

        $response = $this->client->request(
            $requestType,
            $asqa_url . $requestPath,
            array_merge(
                ['auth_basic' => [$asqa_username, $asqa_password]],
                $requestOptions
            )
        );

        $status_code = $response->getStatusCode();
        $response_content = $response->getContent();

        $asqa_response = new JsonResponse([
            "data" => $response_content,
            "status" => $status_code
        ]);

        // Use the JSON_PRETTY_PRINT
        return $asqa_response->setEncodingOptions($asqa_response->getEncodingOptions() | JSON_PRETTY_PRINT);
    }
}
