<?php

namespace App\Controller;

use App\Service\ServerDialog;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AuditController extends AbstractController
{
    /**
     * @var string Referential with its version used to run the audits
     */
    public const REFERENTIAL = "RGAA_4_0";
    /**
     * @var string Level of the referential used to run the audits (could be one of A, AA or AAA ; A and AAA are almost never used)
     */
    public const REFERENTIAL_LEVEL = "AA";
    private ServerDialog $client;

    public function __construct(ServerDialog $client)
    {
        $this->client = $client;
    }

    /**
     *
     * List all audits stored on the server
     *
     * @param ServerDialog $client
     * @return JsonResponse
     */
    public function listAudits(ServerDialog $client): JsonResponse
    {
        return $client->apiCall('GET', '/api/v0/audit', []);
    }

    /**
     *
     * Runs a page audit
     *
     * @param string $url URL to audit. By now only one URL is allowed. $url has to be a valid URI (verification is not made).
     * @param int $contractId Contract to which the audit will be tied to. Default value is 1
     * @param string $tag Tag to label the audit. By now only one tag is allowed. Default value is ""
     * @return JsonResponse
     */
    public function pageAuditRun(ServerDialog $client, string $url, string $tag = "", int $contractId = 1): JsonResponse
    {
        $request_options = [
            'json' => [
                'referential' => AuditController::REFERENTIAL,
                'level' => AuditController::REFERENTIAL_LEVEL,
                'contractId' => $contractId,
                'urls' => [$url],
                'tags' => [$tag]
            ]
        ];

        return $client->apiCall('POST', '/api/v0/audit/page/run', $request_options);
    }
}
