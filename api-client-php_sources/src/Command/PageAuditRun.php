<?php

namespace App\Command;

use App\Controller\AuditController;
use App\Service\ServerDialog;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'asqatasun:page-audit-run',
    description: 'Runs a page audit',
    aliases: ['asqa:page-audit-run'],
    hidden: false
)]
class PageAuditRun extends Command
{
    private AuditController $auditController;
    private LoggerInterface $logger;
    private ServerDialog $client;

    public function __construct(AuditController $auditController, LoggerInterface $logger, ServerDialog $client)
    {
        $this->auditController = $auditController;
        $this->logger = $logger;
        $this->client = $client;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('url', InputArgument::REQUIRED, 'The URL to audit.')
            ->addOption('tag', 't', InputOption::VALUE_REQUIRED, 'Tag to label the audit.', "");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $cmd_io = new SymfonyStyle($input, $output);

        $audit_page = $this->auditController->pageAuditRun($this->client, $input->getArgument('url'), $input->getOption('tag'));
        $status_code = $audit_page->getStatusCode();
        switch (true) {
            case 200 >= $status_code:
                $audit_response = json_decode($audit_page->getContent());
                $cmd_io->success(sprintf('Status code: %s', $status_code));
                $cmd_io->text(sprintf('Audit launched with id: %s', $audit_response->data));
                return Command::SUCCESS;
            case 300 >= $status_code:
                $this->logger->error("Redirection detected");
                $cmd_io->error("Redirection detected");
                return Command::FAILURE;
            case 400 >= $status_code:
                $this->logger->error("Client-side error");
                $cmd_io->error("Client-side error");
                return Command::FAILURE;
            case 500 >= $status_code:
                $this->logger->error("Server error");
                $cmd_io->error("Server-side error");
                return Command::FAILURE;
        }

        $this->logger->error("Unknown status code");
        $cmd_io->error("Unknown status code");
        return Command::FAILURE;
    }
}
