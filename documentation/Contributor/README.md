# Contributor documentation

## Coding standard verification

Please run: `tools/php-cs-fixer fix`

## Static code analysis with PHPStan

```shell
cd api-client-php_sources
vendor/bin/phpstan analyse
```

Config file is `api-client-php_sources/phpstan.neon`
