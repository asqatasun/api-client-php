
# Documentation

For user:

1. [Prerequisites](Prerequisites.md)
2. [Installation](Install.md)
3. [Usage](Usage.md)

For developers:

- [QA tools](QA_tools.md)
