# Installation

From the cloned repository:

```shell
cd api-client-php_sources
composer require symfony/requirements-checker
APP_ENV=prod composer install --no-dev --optimize-autoloader
APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
```

Try with:

```shell
APP_ENV=prod ./bin/console asqatasun:page-audit-run -t "test" https://asqatasun.org/
```
