# Prerequisites

## Ubuntu 22.04

- [Composer](https://getcomposer.org/)

```shell
sudo apt update
sudo apt install  ca-certificates apt-transport-https software-properties-common
sudo apt update
sudo apt install php8.1 php8.1-xml php8.1-zip unzip
```

## Ubuntu 18.04

- [Composer](https://getcomposer.org/)

```shell
sudo apt update
sudo apt install  ca-certificates apt-transport-https software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt install php8.1 php8.1-xml php8.1-zip unzip
```

## Additional prerequisites for developers and CI

- [Phive](https://phar.io) and run `phive install`

Developer tools are managed with Phive, mainly to avoid messing up the `composer.json` file with dependencies.
Phive configuration file is [phive.xml](../phive.xml) and lists tools configured for the project.
To install all them, simply type `phive install` or see [Phive usage](https://phar.io/#Usage)

