# Usage

## Get list of all audits

```shell
./bin/console asqatasun:audit-list
```

## Launch a page audit

```shell
./bin/console asqatasun:page-audit-run --tag "test_campaign_00" https://example.org/
```

## Configuration

Modify `.env` file or better, create copy it to `.env.local` and then modify it. You can:

- modify the URL of Asqatasun Server
- modify your credentials

## Use case: audit lots of pages from a txt file

```shell
URL_FILE="~/urls-list.txt"
COUNTER=0
SLEEP_SECONDS=3
TAG="my_campaign"

while IFS= read -r URL
do
  ((COUNTER++));
  echo "$COUNTER $URL" ;
  ./bin/console asqatasun:page-audit-run -t "${TAG}" "$URL";
  echo ;
  sleep "${SLEEP_SECONDS}s" ;
done < "${URL_FILE}"
```

## For a given tag, count number of audits

```shell
curl \
  --silent \
  -X 'GET' \
  'http://admin%40asqatasun.org:myAsqaPassword@localhost:8080/api/v0/audit/tags/my-tag' \
  -H 'accept: */*' \
  | jq -c '.[]' \
  | wc -l
```

## For a given tag, count number of *not* COMPLETED audits

```shell
curl \
  --silent \
  -X 'GET' \
  'http://admin%40asqatasun.org:myAsqaPassword@localhost:8080/api/v0/audit/tags/my-tag' \
  -H 'accept: */*' \
  | jq -c '.[] | select(.status != "COMPLETED")' \
  | wc -l
```
