# Asqatasun API client in PHP

PHP client for the Asqatasun **API**

## Documentation

1. [Prerequisites](documentation/Prerequisites.md)
2. [Install](documentation/Install.md)
3. [Usage](documentation/Usage.md)

## License

[aGPL v3](LICENSE)
